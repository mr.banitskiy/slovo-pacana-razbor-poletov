package com.series.word.oftheman.info.appc.sok

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.series.word.oftheman.info.appc.R
import com.series.word.oftheman.info.appc.esheest.Bakk
import com.series.word.oftheman.info.appc.esheest.Butilka

@Composable
fun vtoroy(pervoe:()->Unit,vtoroe:()->Unit,tretie:()->Unit, Ma:Activity) {

    val h = LocalConfiguration.current.screenHeightDp
    val w = LocalConfiguration.current.screenWidthDp
    Box(modifier = Modifier.fillMaxSize())
    {
        Bakk(image = R.drawable.menbg)
        Column(modifier = Modifier.align(Alignment.Center).size(height = (h*0.55).dp, width = (w*0.5).dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((h*0.03).dp)) {
            Butilka(perehodik = { pervoe.invoke() }) {
                Image(painter = painterResource(id = R.drawable.po4emutakploho), contentDescription ="" )
            }
            Butilka(perehodik = { vtoroe.invoke() }) {
                Image(painter = painterResource(id = R.drawable.po4emusopopular), contentDescription ="" )
            }
            Butilka(perehodik = { tretie.invoke()}) {
                Image(painter = painterResource(id = R.drawable.vtoroysezon), contentDescription ="" )
            }
           Butilka(perehodik = { Ma.finish() }) {
               Image(painter = painterResource(id = R.drawable.vihod), contentDescription ="" )
           }

        }
    }
    BackHandler {

    }
}