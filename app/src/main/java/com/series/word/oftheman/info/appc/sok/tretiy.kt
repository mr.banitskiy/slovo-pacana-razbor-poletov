package com.series.word.oftheman.info.appc.sok

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.series.word.oftheman.info.appc.R
import com.series.word.oftheman.info.appc.esheest.Bakk
import com.series.word.oftheman.info.appc.esheest.Butilka
import com.series.word.oftheman.info.appc.esheest.Textos

@Composable
fun tretiy(gb:()->Unit) {

    val fh = LocalConfiguration.current.screenHeightDp
    val fw = LocalConfiguration.current.screenWidthDp

    val text = remember{
        mutableListOf(
            "Если вам немного за тридцать и вы росли в типичном спальнике, что приходит на ум при словосочетании «пацанская этика»?" +
                    " Это суровый кодекс чести, предполагающий когда-то даже самопожертвование, когда-то жесткий ответ за слова и, " +
                    "безусловно, уважение к старшим. Как объяснял Роберт, заставший расцвет «казанского феномена», в советские годы" +
                    " пацанская этика была низовым способом противостоять официальной власти и иерархии, формой стихийной оппозиции" +
                    ". Такой кукиш со сбитыми костяшками, спрятанный в кармане болоньевой куртки. Когда надо, кукиш будет извлечен, " +
                    "и посыплются комсомольские зубы. И вроде бы это дела тридцатилетней давности, и давным-давно брат Марата в " +
                    "исполнении Янковского вернулся из Афгана, уже вместо пацанского движа у нас то АУЕ, то ЧВК «Редан», а как" +
                    " глубоко это сидит! И как там, если отбросить романтику, уродливо под этой болоньевой курткой.",
            "Вне зависимости от топонимики по месту взросления вы поймете, о чем я. Конец 1990-х, начало благополучных" +
                    " 2000-х, район метро «Кантемировская»; все ребята уже разделились на рэперов и металлистов." +
                    " Кто-то даже по политическим предпочтениям разошелся по бункерам и интеллигентским кафе, " +
                    "но есть еще на карте места, куда лучше не заходить. У нас это называлось Кубой — улица" +
                    " Бехтерева, куда заказан вход всем тем, кто проживает в районе улиц Веселой, Ереванской," +
                    " Кавказского бульвара и всего, что находится через дорогу. Потому что… А нипочему!\nДворовое" +
                    " детство, куда проникли ветхозаветные пацанские правила, заставляло воспринимать их как аксиому:" +
                    " если идешь с девочкой, то не докопаются, но на обратном пути, когда проводишь, обязательно вломят." +
                    " За длинные волосы получишь вне всяких вопросов. На вопрос «Кто по жизни?» отвечать надо " +
                    "«Человек», но все равно проиграешь, потому что виноват лишь в том, что «хочется мне кушать». " +
                    "Не уверен, что ребята, задававшие вопросы, сами осознавали, ради чего они их спрашивают, откуда" +
                    " оно пошло и что значат все эти поведенческие повадки. Выходит, низовая идея, противостоящая официозу," +
                    " который в позднесоветские годы воспроизводил бессмысленные лозунги, не вдаваясь в их смыслы, тоже" +
                    " превратилась в бесконечное и бессмысленное воспроизводство агрессии. И никакого благородства в этом не было.",
             "И в сериале Крыжовникова, и в книге Гараева, проговаривается, что «пацан не равно мужчина». Значит, истинно" +
                     " мужской кодекс чести допускает двойные трактовки и относится лишь к тем, кто равнее или просто посильне" +
                     "е других. Казалось, в суровом мужском сообществе должна цениться не только сила, но и справедливость:" +
                     " драться надо с равными, нельзя обижать слабых. Иначе в чем доблесть? Однако герои «Слова пацана»" +
                     " утверждают, что слово, данное лоху, ценности не имеет. А чухана (то есть кого угодно) не жаль" +
                     " — ему благородное отношение не положено.\n" +
                     "Этот момент сильнее всего занимает меня в изучении ультрамаскулинных сообществ. " +
                     "В таких объединениях все должно быть построено на сдерживании эмоций, на силе, " +
                     "которая предполагает спокойствие и ответственность, однако участники суровых альянсов" +
                     " на зоне, в армии или во дворе сыпятся на мелочах, легко забывая кодекс и находя оправдания" +
                     " любым действиям. Бить беззащитного? Ну а не надо было оскорблять то, во что я верю!" +
                     " Налететь толпой? А пусть ходит не один! Пацан сказал — пацан сделал? А кто ты такой, чтобы спрашивать?",
            "От поколения, которое выросло на пацанской улице, это уже не отлепишь, мы этим пропитались. " +
                    "Да и не надо отказываться от своего прошлого, от попыток зажмуриться пока было только хуже. " +
                    "Говорить об этом, пытаться переосмысливать, выявлять закономерности и даже пытаться взять " +
                    "что-то хорошее из тех странных правил, конечно, можно. Только, кажется, сделать это смогут " +
                    "совсем не мужчины за тридцать, а люди постарше.\n" +
                    "Я не знаю, какими глазами смотрят «Слово пацана» люди," +
                    " чье детство пришлось на 2010-е, у которых вместо стылого" +
                    " подъезда был фуд-корт районного ТЦ. Зато хорошо понимаю," +
                    " как сериал воспринимаем мы. На премьере чиновники от культуры," +
                    " комментируя сериал, говорили, что у продюсеров получилось наглядное" +
                    " пособие по тому, «как не надо», и что возвращаться в то время ужасно." +
                    " Однако пугающе много его примет можно обнаружить в 2023-м. Не только " +
                    "обострившееся разделение на своих и чужих, но даже дефицит, когда некоторые вещи," +
                    " кхм, приходилось доставать, вернулся в современных реалиях." +
                    " «Черт, только не опять!» — вот главная мысль, выползающая с каждой серией. " +
                    "Особенно когда обнаруживаешь рифмы в беседах героев о брате, который должен вернуться," +
                    " «и уж тут-то начнется». В общем, «никогда снова ходи опасно»."
        )
    }
    val kartinka = remember{
        mutableListOf(
            R.drawable.pervoe1,
            R.drawable.pervoe2,
            R.drawable.pervoe3,
            R.drawable.pervoe4,
        )
    }
    val what = remember{
        mutableStateOf(0)
    }
    Box(modifier = Modifier.fillMaxSize())
    {
        if(what.value == 0)
        {
            Bakk(image = R.drawable.pervoeforpervoe)
        }
        else
        {
            Bakk(image = R.drawable.po4emutakplohosm)
        }
        Butilka(perehodik = { if(what.value==0)
        {
            gb.invoke()
        }
                            else{
                                what.value--
                            }},
            modifier = Modifier.padding(top = (fh*0.03).dp, start = (fw*0.05).dp)) {
            Image(painter = painterResource(id = R.drawable.backbutton), contentDescription ="" )
        }
        Column (modifier = Modifier
            .align(Alignment.Center)
            .height(
                if (what.value == 0) {
                    (fh * 0.6).dp
                } else {
                    (fh * 0.7).dp
                }
            )
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((fh*0.03).dp))
        {
            Image(painter = painterResource(kartinka[what.value]), contentDescription ="",
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillBounds)
            Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
                Textos(text = text[what.value], size = 17)
            }
        }

        if(what.value==3)
        {


            Butilka(perehodik = { gb.invoke()},
                modifier = Modifier
                    .align(
                        Alignment.BottomCenter
                    )
                    .padding(bottom = (fh * 0.1).dp)) {
                Image(painter = painterResource(id = R.drawable.vmenu), contentDescription ="")
            }
        }
        else
        {
            Butilka(perehodik = {
                what.value++
            },
                modifier = Modifier
                    .align(
                        Alignment.BottomCenter
                    )
                    .padding(bottom = (fh * 0.1).dp)) {
                Image(painter = painterResource(id = R.drawable.dalee), contentDescription ="")
            }
        }

    }
    BackHandler {

    }
}