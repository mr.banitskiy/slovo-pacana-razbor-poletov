package com.series.word.oftheman.info.appc.sok

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.series.word.oftheman.info.appc.R
import com.series.word.oftheman.info.appc.esheest.Bakk
import com.series.word.oftheman.info.appc.esheest.Butilka
import com.series.word.oftheman.info.appc.esheest.Textos

@Composable
fun Pyatiy(gb:()->Unit) {
    val fh = LocalConfiguration.current.screenHeightDp
    val fw = LocalConfiguration.current.screenWidthDp

    val text1 = remember{
        mutableListOf(
           "Будет ли второй сезон сериала\n           «Слово пацана»",
            "О чем может быть второй сезон\n            «Слова пацана»",
            "Почему «Слово пацана» хотели\n               запретить?",
            "Интересные факты о сериале\n          «Слово пацана»",
            "Актерский состав первого сезона\n                     сериала"


        )
    }
    val text2 = remember{
        mutableListOf(
            "Пока авторы проекта не дали точного ответа на вопрос, будет ли сериал «Слово пацана» продлен на" +
                    " второй сезон. Слухи о кастингах для актеров второго сезона создатели проекта опровергают.\n" +
                    "При этом ранее режиссер Жора Крыжовников говорил, что «Слово пацана. Кровь на асфальте»" +
                    " покажет историю развития криминальных банд, и проект планируется разделить на три сезона." +
                    " Однако бешеная популярность первого сезона, возможно, заставит авторов сериала пересмотреть" +
                    " свои планы.\nСлова режиссера в целом подтверждает и сценарист Андрей Золотарев. Он настаивал " +
                    "на создании продолжения по примеру культовой картины «Крестный отец», которая также состоит из" +
                    " трех фильмов.\n" +
                    "Тем временем Иван Янковский, исполнитель роли Вовы Адидаса, уверяет своих подписчиков, " +
                    "что работа над вторым сезоном еще не идет. А в пресс-службе онлайн-кинотеатра Wink" +
                    " рассказали, что съемки второго сезона «Слова пацана» начнутся не раньше января 2024" +
                    " года.\nЧто касается места съемок, в Сети ходят слухи, что в новых сериях на экране" +
                    " засветятся улицы Курска, Липецка, Перми или даже Калининграда. Возможно, в центре" +
                    " сюжета окажутся уже не казанские пацаны, а участники группировок из других городов.",
            "Учитывая, что продление проекта на второй сезон пока под вопросом, стоит только догадываться, " +
                    "какую историю расскажут создатели в новых сериях.\n" +
                    "Есть версия, что действие будет происходить уже не в конце" +
                    " 80-х годов, а немного позднее — когда Андрей выйдет из колонии " +
                    "для несовершеннолетних и «пришьется» к другой банде. А может быть," +
                    " он даже возглавит «Универсам».\nКак известно, в 90-х подростковые группировки" +
                    " переросли в организованную преступность и начали решать свои конфликты уже не " +
                    "кулаками, а реальным оружием. Поэтому возникают сомнения, что авторы захотят" +
                    " воплотить такой вариант сценария — первый сезон уже предлагали запретить за" +
                    " то, что «Слово пацана» якобы романтизирует насилие и толкает молодежь на преступления.",
            "Несмотря на огромную популярность, сериал «Слово пацана» буквально разделил аудиторию на два лагеря: тех," +
                    " кому проект понравился, и тех, кто потребовал его запретить.\n" +
                    "В итоге шоу даже проверял Роскомнадзор. Ведомство не нашло в сериале нарушений, и «Слово пацана» " +
                    "под запрет не попало. Кроме того, положительную оценку сериалу дали и в Совете Федерации." +
                    " По словам сенатора Ивана Евстифеева, проект достоверно отражает советскую реальность и в" +
                    " первую очередь напоминает зрителям о том, что мрачные события 80-90-х годов не должны повториться.\n" +
                    "Против запрета выступил и режиссер Никита Михалков. Он отметил, что воспитание детей — это" +
                    " в первую очередь ответственность родителей. Так что если подростки становятся похожими на" +
                    " героев сериала, значит, взрослые в их семье не справляются со своей задачей.",
            "• Хоть сериал и снят по книге, все его герои вымышлены. В то же время в биографии Андрея прослеживаются моменты " +
                    "из жизни Роберта " +
                    "Гараева: автор романа до вступления в группировку тоже был обычным школьником, занимался творчеством," +
                    " а в местной банде искал защиту — один из приятелей Гараева вымогал у него деньги.\n" +
                    "• После того, как в Сеть слили рабочий вариант седьмой серии проекта, исполнитель одной из " +
                    "главных ролей Иван Янковский публично назвал злоумышленников чушпанами.\n" +
                    "• Так как действие сериала происходит в Казани, то и снимать картину должны были в этом городе. " +
                    "Однако местные власти не разрешили съемки, так что героям пришлось наводить ужас на улицы Ярославля" +
                    " — именно там снимали первый сезон.\n" +
                    "• Для многих молодых актеров проект стал настоящим прорывом. Так," +
                    " Рузиль Минекаев (Марат) до сериала играл только в эпизодах," +
                    " Леон Кемстач (Андрей) прежде снимался в кино лишь один раз," +
                    " а родители Анны Пересильд (Айгуль) — актриса Юлия Пересильд и" +
                    " режиссер Алексей Учитель — и вовсе хотели запретить девочке участвовать" +
                    " в сериале, ее роль они сочли жуткой.\n",
            "• Иван Янковский — Вова Адидас, глава группировки «Универсам»;\n" +
                    "• Рузиль Минекаев — Марат (Адидас-младший), брат Вовы, также состоит в «Универсаме»;\n" +
                    "• Леон Кемстач — Андрей Васильев по прозвищу Пальто, 14-летний подросток, новый член группировки «Универсам»;\n" +
                    "• Никита Кологривый — Кощей, бывший лидер банды «Универсам»;\n" +
                    "• Анна Пересильд — Айгуль Ахмерова, девушка Марата;\n" +
                    "• Анастасия Красовская — младший лейтенант милиции Ирина Сергеевна;\n" +
                    "• Сергей Бурунов — Кирилл Суворов, отец Вовы и Марата;\n" +
                    "• Юлия Александрова — Светлана Михайловна, мама Андрея;\n" +
                    "• Антон Васильев — майор милиции Ильдар Юнусович."

        )
    }
    val kartinka = remember{
        mutableListOf(
            R.drawable.tretiy1,
            R.drawable.tretiy2,
            R.drawable.tretiy3,
            R.drawable.tretiy4,
            R.drawable.tretiy5,
        )
    }
    val what = remember{
        mutableStateOf(0)
    }
    Box(modifier = Modifier.fillMaxSize())
    {

        Bakk(image = R.drawable.tretiybg)

        Butilka(perehodik = { if(what.value==0)
        {
            gb.invoke()
        }
        else{
            what.value--
        }},
            modifier = Modifier.padding(top = (fh*0.03).dp, start = (fw*0.025).dp)) {
            Image(painter = painterResource(id = R.drawable.backbutton), contentDescription ="" )
        }
        Column (modifier = Modifier
            .align(Alignment.Center)
            .padding(bottom = (fh*0.1).dp)
            .height((fh*0.7).dp)
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((fh*0.012).dp))
        {
            Textos(text = text1[what.value], size = 17)
            Image(painter = painterResource(kartinka[what.value]), contentDescription ="",
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillBounds)
            Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
                Textos(text = text2[what.value], size = 17)
            }
        }

        if(what.value==4)
        {


            Butilka(perehodik = { gb.invoke()},
                modifier = Modifier
                    .align(
                        Alignment.BottomCenter
                    )
                    .padding(bottom = (fh * 0.1).dp)) {
                Image(painter = painterResource(id = R.drawable.vmenu), contentDescription ="")
            }
        }
        else
        {
            Butilka(perehodik = {
                what.value++
            },
                modifier = Modifier
                    .align(
                        Alignment.BottomCenter
                    )
                    .padding(bottom = (fh * 0.1).dp)) {
                Image(painter = painterResource(id = R.drawable.dalee), contentDescription ="")
            }
        }

    }
    BackHandler {

    }
}