package com.series.word.oftheman.info.appc.sok

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import com.series.word.oftheman.info.appc.R
import com.series.word.oftheman.info.appc.esheest.Bakk
import kotlinx.coroutines.delay

@Composable
fun perviy(gNExt:()->Unit) {

    LaunchedEffect(key1 = Unit)
    {
        delay(3000)
        gNExt.invoke()
    }

    Box(modifier = Modifier.fillMaxSize() )
    {
        Bakk(image = R.drawable.loadingbg)
    }
}