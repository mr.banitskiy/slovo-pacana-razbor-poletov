package com.series.word.oftheman.info.appc.esheest

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource

@Composable
fun Bakk(image: Int) {
    Image(painter = painterResource(id = image), contentDescription ="",
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.FillBounds)
}