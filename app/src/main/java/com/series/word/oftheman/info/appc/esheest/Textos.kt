package com.series.word.oftheman.info.appc.esheest

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.series.word.oftheman.info.appc.R
import com.series.word.oftheman.info.appc.ui.theme.Black
import com.series.word.oftheman.info.appc.ui.theme.White

@Composable
fun Textos(text:String,size:Int) {

    Box(){
        Text(text = text, fontSize = size.sp,
            fontFamily = FontFamily(Font(R.font.bimbim)),
            color = Black,modifier =  Modifier.offset(x = 1.dp, y = 1.dp),
            fontWeight = FontWeight.Bold)
        Text(text = text, fontSize = size.sp,
            fontFamily = FontFamily(Font(R.font.bimbim)),
            color = Black,modifier =  Modifier.offset(x = (-1).dp, y = (-1).dp),
            fontWeight = FontWeight.Bold)
        Text(text = text, fontSize = size.sp, color = White,
            fontFamily = FontFamily(Font(R.font.bimbim)),
            fontWeight = FontWeight.Bold)
    }

}