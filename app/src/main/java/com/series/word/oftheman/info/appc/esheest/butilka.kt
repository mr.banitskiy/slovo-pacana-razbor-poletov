package com.series.word.oftheman.info.appc.esheest

import android.view.MotionEvent
import androidx.compose.animation.core.Animatable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInteropFilter
import kotlinx.coroutines.launch

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Butilka(
    perehodik: () -> Unit,
    modifier: Modifier = Modifier,
    nudagfdkgdf: @Composable () -> Unit
) {
    val animaciya = remember { Animatable(1f) }
    val toje = rememberCoroutineScope()
    Box(
        modifier = Modifier
            .then(modifier)
            .scale(animaciya.value)
            .pointerInteropFilter {
                when (it.action) {
                    MotionEvent.ACTION_DOWN -> {
                        toje.launch {
                            animaciya.animateTo(0.8f)
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        toje.launch {
                            animaciya.animateTo(1f, block = { if (animaciya.value == 1f)
                            { perehodik.invoke() } })
                        }
                    }
                }
                true
            }, contentAlignment = Alignment.Center
    ) {
        nudagfdkgdf.invoke()
    }

}
