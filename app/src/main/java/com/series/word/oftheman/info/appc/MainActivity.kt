package com.series.word.oftheman.info.appc

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.series.word.oftheman.info.appc.esheest.pipi
import com.series.word.oftheman.info.appc.sok.Chetvertiy
import com.series.word.oftheman.info.appc.sok.Pyatiy
import com.series.word.oftheman.info.appc.sok.perviy
import com.series.word.oftheman.info.appc.sok.tretiy
import com.series.word.oftheman.info.appc.sok.vtoroy
import com.series.word.oftheman.info.appc.ui.theme.СловоПацанаРазборПолетовTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val shitka = WindowCompat.getInsetsController(window, window.decorView)
        shitka.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        shitka.hide(WindowInsetsCompat.Type.statusBars())
        WindowCompat.setDecorFitsSystemWindows(window,false)
        setContent {
            СловоПацанаРазборПолетовTheme {
                val nav = rememberNavController()

                NavHost(navController = nav, startDestination = pipi(quest = "ebanashka") )
                {
                    composable(pipi(quest = "ebanashka"))
                    {
                        perviy(gNExt = {nav.navigate(pipi(quest = "eshe"))})
                    }
                    composable(pipi(quest = "eshe"))
                    {
                        vtoroy(
                            pervoe = { nav.navigate(pipi("raz")) },
                            vtoroe = {  nav.navigate(pipi("dva")) },
                            tretie = {  nav.navigate(pipi("tri")) },
                            Ma = this@MainActivity
                        )
                    }
                    composable(pipi("raz"))
                    {
                        tretiy (gb = {nav.popBackStack()})
                    }
                    composable(pipi("dva"))
                    {
                        Chetvertiy(gb = { nav.popBackStack() })
                    }
                    composable(pipi("tri"))
                    {
                        Pyatiy(gb = { nav.popBackStack() })
                    }

                }
            }
        }
    }
}
